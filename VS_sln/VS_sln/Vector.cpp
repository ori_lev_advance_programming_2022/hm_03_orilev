#include "Vector.h"
#include <string>


#define MIN_VECTOR_SIZE 2
#define EMPTY_VECTOR 0
#define EMPTY_VECTOR_CODE -9999


Vector::Vector(int n)
{
	// if the starting-vector-lenght is less then 2, its defult is 2..
	if (n < MIN_VECTOR_SIZE) { n = MIN_VECTOR_SIZE; }

	this->_capacity = n;
	this->_resizeFactor = n;
	this->_size = 0;

	this->_elements = new int[this->_capacity];
}

Vector::~Vector()
{
	delete[] this->_elements;

	// prevente risks
	this->_elements = nullptr;
}


int Vector::resizeFactor() const { return this->_resizeFactor; }

int Vector::size() const { return this->_size; }

int Vector::capacity() const { return this->_capacity; }

bool Vector::empty() const { return this->_size == EMPTY_VECTOR; }

int Vector::pop_back()
{
	// if there are items in the vector..
	if (this->_size > EMPTY_VECTOR)
	{
		// dec the ptr to the array in one, and then return the value..
		return this->_elements[--this->_size];
	}

	std::cerr << "error: pop from empty vector" << std::endl;
	return EMPTY_VECTOR_CODE;
}

void Vector::reserve(const int n)
{
	int new_capacity = this->_capacity;
	int* temp_elements = nullptr;
	// don't need to increase the vector..
	if (n <= this->_capacity) { return; }

	// calculate the new capacity
	// while the new capacity is less then the new lenght
	// added to the capacity the resize factor...
	// if n = 23
	// old capacity = 10, resizeFactor = 5
	// new capacity = 25, like the pdf....
	while (new_capacity < n) { new_capacity += this->_resizeFactor; }

	this->_capacity = new_capacity;

	// save the current vector elements
	temp_elements = this->_elements;

	// create new array with bigger capacity
	this->_elements = new int[this->_capacity];

	// copied the old vector values to the current vector values.
	// loop till this._size, its still the current len of the values....
	for (std::size_t i = 0; i < this->_size; ++i)
	{
		this->_elements[i] = temp_elements[i];
	}

	delete[] temp_elements;
	temp_elements = nullptr;
}

void Vector::push_back(const int& val)
{
	// inc the array if needed....
	if (this->_size >= this->_capacity)
	{
		this->reserve(this->_capacity + this->_resizeFactor);
	}

	// put the new value in the array, and the increace the size ptr...
	this->_elements[this->_size++] = val;
}


void Vector::resize(const int n)
{
	// what the point of this function???
	this->reserve(n);
	this->_size = n;
}


void Vector::assign(const int val)
{
	for (std::size_t i = 0; i < this->_size; ++i)
		this->_elements[i] = val;
}


void Vector::resize(const int n, const int& val)
{
	// because resize function change this.size....
	int old_size = this->_size;

	this->resize(n);

	for (std::size_t i = 0; i < old_size; ++i)
		this->_elements[i] = val;
}


Vector::Vector(const Vector& other)
{
	// copied the simle data
	this->_size = other.size();
	this->_capacity = other.capacity();
	this->_elements = nullptr;
	this->_resizeFactor = other.resizeFactor();

	// clone the elemenets
	// i < this->_size, its the same size as the other.size, but you dont call other.size() in any loop....
	
	this->_elements = new int[this->_capacity];

	for (std::size_t i = 0; i < this->_size; ++i)
	{
		this->_elements[i] = other[i];
		//this->_elements[i] = other._elements[i];
	}
}


Vector& Vector::operator=(const Vector& other)
{
	// tries to copy the object to itself
	if (this == &other) { return *this; }

	// prevente memory leaks.
	delete[] this->_elements; // release old memory

	// copy simple data
	this->_size = other.size();
	this->_resizeFactor = other.resizeFactor();
	this->_capacity = other.capacity();

	// create new dynamic array and copy the arrays...
	this->_elements = new int[this->_capacity];

	for (std::size_t i = 0; i < this->_size; ++i)
		this->_elements[i] = other._elements[i];
	
	return *this;
}


int& Vector::operator[](int n) const
{
	if (n >= EMPTY_VECTOR && n < this->_size)
	{
		return this->_elements[n];
	}
	std::cerr << "[*] ERROR: make sure that the index in the vecrot range." << std::endl;
	return this->_elements[EMPTY_VECTOR];
}


Vector& Vector::operator+=(const Vector& other)
{
	for (std::size_t i = 0; i < this->_size; ++i)
	{
		// add if its not out of the range of v2, if not add 0 (do nothing)
		if (i > other.size()) {}
		else
		{
			this->_elements[i] += other[i];
		}
	}
	return *this;
}


Vector Vector::operator+(const Vector& other) const
{
	Vector v3(*this);
	v3 += other;
	return v3;
}

Vector& Vector::operator-=(const Vector& other)
{
	for (std::size_t i = 0; i < this->_size; ++i)
	{
		// add if its not out of the range of v2, if not add 0 (do nothing)
		if (i > other.size()) {}
		else { this->_elements[i] -= other[i]; }
	}
	return *this;
}

Vector Vector::operator-(const Vector& other) const
{
	Vector v3(*this);
	v3 -= other;
	return v3;
}

std::ostream& operator<<(std::ostream& os, const Vector& v)
{
	os << "Vector Info : \nCapacity is ";
	os << std::to_string(v.capacity()) << "\n" << "Size is " << std::to_string(v.size()) << " data is{";

	for(std::size_t i = 0; i < v._size; ++i)
	{ 
		if (i + 1 == v._size) 
			os << v[i]; 
		else
			os << v[i] << ", "; 
	}
	os << "}\n";
	return os;
}